SQL DQL Test
---------------------------------------------------------------
1. select employees in descending order - salary
Ans: select * from employee ORDER BY employee salary DESC; 
| id | name     | contact_number | address   | salary | employee_id | role                           | created_at          | updated_at          |
+----+----------+----------------+-----------+--------+-------------+--------------------------------+---------------------+---------------------+
| 11 | preeti   | 9833910513     | noida     |  87000 |       98832 | reconciliation of transactions | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
|  8 | anjali   | 9833910511     | ahmedabad |  60000 |       98828 | reconciliation of transactions | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
| 10 | varun    | 9833910512     | mehsana   |  56000 |       98831 | reconciliation of transactions | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
|  7 | sneha    | 9833910510     | baroda    |  55000 |       98827 | reconciliation of transactions | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
|  6 | namita   | 9833910539     | surat     |  52000 |       98820 | reconciliation of transactions | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
|  5 | pooja    | 9833910538     | delhi     |  50000 |       98826 | reconciliation of transactions | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
|  4 | mareena  | 9833910537     | meerut    |  45000 |       98825 | revaluation of transactions    | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
|  3 | pankaj   | 9833910536     | bhopal    |  40000 |       98824 | valuing of transactions        | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
|  2 | mina     | 9833910535     | thane     |  32000 |       98823 | mangagement of transactions    | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
|  1 | nehav    | 9833910534     | mumbai    |  30000 |       98821 | reconciliation of transactions | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
| 12 | madhu    | 9833910525     | bangalore |  22000 |       98833 | reconciliation of transactions | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
|  9 | harsha   | 9833910512     | mumbai    |  20000 |       98829 | reconciliation of transactions | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
+----+----------+----------------+-----------+--------+-------------+--------------------------------+---------------------+---------------------+
2. select all employees from Mumbai
Ans: select * from employee where address = 'mumbai';
+----+---------+----------------+---------+--------+-------------+--------------------------------+---------------------+---------------------+
| id | name    | contact_number | address | salary | employee_id | role                           | created_at          | updated_at          |
+----+---------+----------------+---------+--------+-------------+--------------------------------+---------------------+---------------------+
|  1 | nehav   | 9833910534     | mumbai  |  30000 |       98821 | reconciliation of transactions | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
|  9 | harsha  | 9833910512     | mumbai  |  20000 |       98829 | reconciliation of transactions | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
+----+---------+----------------+---------+--------+-------------+--------------------------------+---------------------+---------------------+
3. select all employees having salary more than average salary
Ans: select * from employee where salary > All(select avg(salary) from employee); 
+----+---------+----------------+-----------+--------+-------------+--------------------------------+---------------------+---------------------+
| id | name    | contact_number | address   | salary | employee_id | role                           | created_at          | updated_at          |
+----+---------+----------------+-----------+--------+-------------+--------------------------------+---------------------+---------------------+
|  5 | pooja   | 9833910538     | delhi     |  50000 |       98826 | reconciliation of transactions | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
|  6 | namita  | 9833910539     | surat     |  52000 |       98820 | reconciliation of transactions | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
|  7 | sneha   | 9833910510     | baroda    |  55000 |       98827 | reconciliation of transactions | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
|  8 | anjali  | 9833910511     | ahmedabad |  60000 |       98828 | reconciliation of transactions | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
| 10 | varun   | 9833910512     | mehsana   |  56000 |       98831 | reconciliation of transactions | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
| 11 | preeti  | 9833910513     | noida     |  87000 |       98832 | reconciliation of transactions | 2021-05-27 00:00:00 | 2021-05-27 00:00:00 |
+----+---------+----------------+-----------+--------+-------------+--------------------------------+---------------------+---------------------+
4. select sum of salary from table
Ans: select sum(salary) from employee;
+-------------+
| sum(salary) |
+-------------+
|      549000 |
+-------------+
5. select all unique address
Ans: select distinct address from employee;
+-----------+
| address   |
+-----------+
| mumbai    |
| thane     |
| bhopal    |
| meerut    |
| delhi     |
| surat     |
| baroda    |
| ahmedabad |
| mehsana   |
| noida     |
| bangalore |
+-----------+
6. select details, salary from table
(details should be concatenation of name and address)
7. select names of employees having max salary
Ans:SELECT name, MAX(salary) AS salary FROM employee WHERE salary = (SELECT MAX(salary) FROM employee);
+---------+--------+
| name    | salary |
+---------+--------+
| preeti  |  87000 |
+---------+--------+
8. select employees having 2nd max salary
Ans: select max(salary)as salary from employee where salary <(select max(salary)from employee);
+--------+
| salary |
+--------+
|  60000 |
+--------+
9. count emplyees by address, order by employee count
Ans:select address,count(*) from employee group by address;
+-----------+----------+
| address   | count(*) |
+-----------+----------+
| ahmedabad |        1 |
| bangalore |        1 |
| baroda    |        1 |
| bhopal    |        1 |
| delhi     |        1 |
| meerut    |        1 |
| mehsana   |        1 |
| mumbai    |        2 |
| noida     |        1 |
| surat     |        1 |
| thane     |        1 |
+-----------+----------+
e.g. select count(name,address) from employee
---------------------------
city | emplyee count |
---------------------------
Mumbai |   4    |
Nagpur |    3    |
Pune |    2    |
---------------------------
10. count employees from mumbai only
Ans: SELECT count( * ) as total_record FROM employee WHERE address = 'mumbai'
+--------------+
| total_record |
+--------------+
|            2 |
+--------------+
11. select all employees whose names starts or ends with vowels
Ans:select * from employee where left(name,1) in (‘a’,’e’,’i’,’o’,’u’)and right(name,1)in (‘a’,’e’,’i’,’o’,’u’); 
12. find employee having max salary in particular city
Ans:select name,address, max(Salary) from employee group by name,address
+----------+-----------+-------------+
| name     | address   | max(salary) |
+----------+-----------+-------------+
| anjali   | ahmedabad |       60000 |
| harsha   | mumbai    |       20000 |
| madhu    | bangalore |       22000 |
| mareena  | meerut    |       45000 |
| mina     | thane     |       32000 |
| namita   | surat     |       52000 |
| nehav    | mumbai    |       30000 |
| pankaj   | bhopal    |       40000 |
| pooja    | delhi     |       50000 |
| preeti   | noida     |       87000 |
| sneha    | baroda    |       55000 |
| varun    | mehsana   |       56000 |
+----------+-----------+-------------+
e.g.
-------------------------------------
address  | name  | salary |
------------------------------------|
noida   | preeti  | 87000 |
ahmedabad  | anjali  | 60000 |
bangalore  | madhu  | 60000 |
baroda   | varun  | 56000 |
-------------------------------------